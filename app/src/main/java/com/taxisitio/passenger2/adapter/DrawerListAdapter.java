package com.taxisitio.passenger2.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.taxisitio.passenger2.R;
import com.taxisitio.passenger2.activity.BaseActivity;
import com.taxisitio.passenger2.activity.MainActivity;

public class DrawerListAdapter extends ArrayAdapter<String> {
    BaseActivity context;
    MainActivity activity;
    Integer[] integers;

    public DrawerListAdapter(BaseActivity context, String[] objects, Integer[] drawerListicon, MainActivity activity) {
        super(context, 0, objects);
        this.context = context;
        this.activity = activity;
        this.integers = drawerListicon;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = context.inflater.inflate(R.layout.drawer_list_item, null);
            holder = new ViewHolder();
            holder.titleTV = (TextView) convertView.findViewById(R.id.titleTV);
            holder.imageIV = (ImageView) convertView.findViewById(R.id.imageIV);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.title = getItem(position);
        holder.icon = integers[position];
        holder.titleTV.setText("" + holder.title);

        holder.imageIV.setImageResource(holder.icon);
        return convertView;
    }

    public class ViewHolder {
        String title;
        TextView titleTV;
        ImageView imageIV;
        int icon;
    }
}

R_DEF: Internal format may change without notice
local
attr? edgeSlide
attr? edgeSlideWidth
attr? interpolator
attr? layout_role
attr? primaryShadowDrawable
attr? primaryShadowWidth
attr? secondaryShadowDrawable
attr? secondaryShadowWidth
attr? slideDirection
attr slideMenuStyle
id content
id primaryMenu
id secondaryMenu
styleable SlideMenu primaryShadowWidth secondaryShadowWidth primaryShadowDrawable secondaryShadowDrawable edgeSlide edgeSlideWidth slideDirection interpolator
styleable SlideMenu_Layout layout_role
